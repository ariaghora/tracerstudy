<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
class AlumniModel extends Eloquent {

	protected $fillable = array('id_alumni', 'nim', 'nama_lengkap', 'tgl_lahir', 'prodi', 'fakultas', 'status');
	protected $table = 'data_alumni';

	public $timestamps = false;
}