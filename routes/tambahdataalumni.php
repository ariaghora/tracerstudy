<?php

 //Akses database
/*$servername = "localhost";
$username = "root";
try {
	$conn = new PDO("mysql:host=$servername;dbname=alumni", $username);
    // set the PDO error mode to exception
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	echo "Connected successfully<br>"; 
}
catch(PDOException $e)
{
	echo "Connection failed: " . $e->getMessage(). "<br>";
}


//hitung jumlah data -> check auto-increment number pada database (tabel id_alumni)
$stmt = $conn->query('SELECT * FROM data_alumni');
$row_count = $stmt->rowCount();

//generate 3 digit id -> urutan pendaftaran yang dicocokkan dengan auto-increment number pada database (tabel id_alumni)
$id_alumni = str_pad($row_count+1, 3, '0', STR_PAD_LEFT);

$nim=$_POST['nim'];
$tgl_lahir=$_POST['date'];
$nama_lengkap=strtolower($_POST['nama']);

$nama=substr("$nama_lengkap",0,2);
$angkatan=substr("$nim",2,2);

//generate username dan password. Username 2 digit nama, 3 digit ID dan 2 digit tahun angkatan 
$username= $nama . $id_alumni . $angkatan;
$password=substr($tgl_lahir,8,2).substr($tgl_lahir,5,2).substr($tgl_lahir,0,4);

print 'Username anda : ' . $username . '<br>';
print 'Password anda : ' . $password;

//enkripsi password md5 + hexadecimal
$hex = hexdec(substr(md5("$password"), 0, 16));

//input ke dalam database tabel alumni dan user ~Prodi, fakultas dan status masih NULL~
$conn->query("INSERT INTO data_alumni VALUES('$username', '$nim', '$nama_lengkap', '$tgl_lahir', NULL, NULL, NULL)");
$conn->query("INSERT INTO data_user VALUES ('$username', '$hex', 0)");
*/

$app->get('/tambahdataalumni', function() use($app) {	
	$app->render('formtambah.php');
});

$app->post('/tambahdataalumni', function() use($app) {	
	// dapatkan post variables
	$nim         = $app->request()->post('nim');
	$tglLahir    = $app->request()->post('date');
	$namaLengkap = strtolower($app->request()->post('nama'));

	$count       = AlumniModel::all()->count();
	$idAlumni    = str_pad($count + 1, 3, '0', STR_PAD_LEFT);
	$nama        = substr($namaLengkap, 0, 2);
	$angkatan    = substr($nim, 2, 2);
	$username    = $nama . $idAlumni . $angkatan;
	$password    = substr($tglLahir, 8, 2).substr($tglLahir, 5, 2).substr($tglLahir, 0, 4);

	// entri alumni baru
	$newAlumni               = new AlumniModel();
	$newAlumni->id_alumni    = $username;
	$newAlumni->nim          = $nim;
	$newAlumni->nama_lengkap = $namaLengkap;
	$newAlumni->tgl_lahir    = $tglLahir;
	$newAlumni->prodi        = '';
	$newAlumni->fakultas     = '';
	$newAlumni->status       = '1';
	$newAlumni->save();

	// sekaligus entri user baru
	$newUser           = new UserModel();
	$newUser->username = $username;
	$newUser->password = hexdec(substr(md5("$password"), 0, 16));
	$newUser->otoritas = 0;
	$newUser->save();

});