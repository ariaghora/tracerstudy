<?php
/**
 * Mengarahkan pengguna ke halaman login jika masih
 * belum melakukan login sebelumnya. Jika sudah login 
 * pengguna diarahkan ke beranda.
 */
$app->get('/login', function() use($app){	
	if (isset($_SESSION['logged_in'])) {
		$app->redirect($app->urlFor('root')); // redirect ke beranda
	} else {
		$app->render('login.php');		
	}
});

/**
 * Metode untuk melakukan login
 * @param: username, password 
 */
$app->post('/login', function() use($app){	
	$username = $app->request->post('username');
	$password = $app->request->post('password');	

	$count = UserModel::where('username', '=', "$username")
						->where('password', '=', hexdec(substr(md5($password), 0, 16)))
						->count();

	if ($count == 1) {		
		$response = array("message" => "login berhasil", "data" => "1");
		$_SESSION['logged_in'] = true;
		$_SESSION['username']  = $username;
	} else {
		$response = array("message" => "login gagal", "data" => "0");
	}
	print json_encode($response);
});

$app->get('/logout', function(){
	session_unset();
	session_destroy();
	$app->redirect($app->urlFor('root')); // redirect ke beranda
});